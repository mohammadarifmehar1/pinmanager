package com.docomodigital.pinmanager.repository;

import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.docomodigital.pinmanager.entity.PINStatus;

@Repository
public interface PINStatusRepository extends JpaRepository<PINStatus, String>{
	public void deleteByCreateTimeBefore(LocalDateTime expiryTime);
}
