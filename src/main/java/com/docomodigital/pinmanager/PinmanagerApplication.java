package com.docomodigital.pinmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PinmanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PinmanagerApplication.class, args);
	}

}
