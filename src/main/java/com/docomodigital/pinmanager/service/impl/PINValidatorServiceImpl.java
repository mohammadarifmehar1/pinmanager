package com.docomodigital.pinmanager.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.docomodigital.pinmanager.entity.MSISDNEntity;
import com.docomodigital.pinmanager.entity.PINStatus;
import com.docomodigital.pinmanager.repository.MSISDNRepository;
import com.docomodigital.pinmanager.service.PINValidatorService;

@Service
public class PINValidatorServiceImpl implements PINValidatorService {

	@Autowired
	private MSISDNRepository msisdnRepository;

	@Override
	public boolean validate(String msisdn, char[] pin) {
		MSISDNEntity entity = msisdnRepository.findByMsisdn(msisdn);
		List<PINStatus> list = entity.getPinStatus();
		if (Optional.ofNullable(list).isPresent() && !list.isEmpty()) {
			for (PINStatus status : list) {
				if (String.valueOf(status.getPin()).equals(String.valueOf(pin))) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean validate(String msisdn) {
		if (!Optional.ofNullable(msisdn).isPresent()) {
			return false;
		}
		if (!msisdn.matches("\\d{11}"))
			return false;

		MSISDNEntity entity = msisdnRepository.findByMsisdn(msisdn);
		if (entity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Override
	public boolean validateMaxAttempt(String msisdn) {
		MSISDNEntity entity = msisdnRepository.findByMsisdn(msisdn);
		List<PINStatus> list = entity.getPinStatus();
		if (Optional.ofNullable(list).isPresent()&&list.size() >= 3) {
			return false;
		}
		return true;
	}

}
