package com.docomodigital.pinmanager.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.docomodigital.pinmanager.entity.MSISDNEntity;
import com.docomodigital.pinmanager.entity.PINStatus;
import com.docomodigital.pinmanager.exception.PINManagerException;
import com.docomodigital.pinmanager.repository.MSISDNRepository;
import com.docomodigital.pinmanager.service.PINService;
import com.docomodigital.pinmanager.service.PINValidatorService;

@Service
public class PINServiceImpl implements PINService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PINServiceImpl.class);

	private static final String NUMBERS = "0123456789";

	private static final String VALIDATED_STRING = "Validated";
	private static final int LENGTH = 4;

	private static final String REJECTED_STRING = "Rejected";

	@Autowired
	private MSISDNRepository pinRepository;

	@Autowired
	private PINValidatorService pinValidatorService;

	Random random = new Random();

	@Override
	public char[] generatePIN(String msisdn) throws PINManagerException {
		LOGGER.info("PIN generation started for msisdn:{}", msisdn);
		if (!pinValidatorService.validate(msisdn)) {
			LOGGER.error("MSISDN :{} validation failed", msisdn);
			throw new PINManagerException("MSISDN not found, Please enter a valid MSISDN");
		}
		if (!pinValidatorService.validateMaxAttempt(msisdn)) {
			LOGGER.error("PIN generation attempted 3 times for msisdn:{}", msisdn);
			throw new PINManagerException("You already attempted 3 times");
		}
		char[] pin = new char[LENGTH];

		for (int i = 0; i < LENGTH; i++) {
			pin[i] = NUMBERS.charAt(random.nextInt(NUMBERS.length()));
		}
		MSISDNEntity entity = pinRepository.findByMsisdn(msisdn);
		if (pin.length==4) {
			entity.setMsisdn(msisdn);
			List<PINStatus> list = entity.getPinStatus();
			if (!Optional.ofNullable(list).isPresent()) {
				list = new ArrayList<>();
			}
			PINStatus status = new PINStatus(pin, "Created", LocalDateTime.now());
			if (list.size() < 3) {
				list.add(status);
				entity.setPinStatus(list);
				pinRepository.save(entity);
			} else {
				return new char[] {};
			}
		}
		return pin;
	}

	@Override
	public boolean validatePIN(String msisdn, String pin) {
		if (!Optional.ofNullable(msisdn).isPresent() || !Optional.ofNullable(pin).isPresent()) {
			return false;
		}
		LOGGER.info("PIN validation started for msisdn:{}", msisdn);
		MSISDNEntity entity = pinRepository.findByMsisdn(msisdn);
		List<PINStatus> pinList = entity.getPinStatus();
		String state = "";
		if (Optional.ofNullable(pinList).isPresent()) {
			for (PINStatus status : pinList) {
				if (pin.equals(String.valueOf(status.getPin())) && !status.getStatus().equals(VALIDATED_STRING)
						&& !status.getStatus().equals(REJECTED_STRING)) {
					LOGGER.info("PIN validated successfully for msisdn:{}", msisdn);
					status.setStatus(VALIDATED_STRING);
					state = VALIDATED_STRING;
					break;
				}
			}
			if (state.equals(VALIDATED_STRING)) {
				for (PINStatus status : pinList) {
					if (!status.getStatus().equals(VALIDATED_STRING)) {
						status.setStatus(REJECTED_STRING);
					}
				}
				pinRepository.save(entity);
				LOGGER.info("PIN Status updated into db");
				return true;
			}
		}
		return false;
	}
}
