package com.docomodigital.pinmanager.service;

public interface PINValidatorService {

	public boolean validate(String msisdn, char[] pin);

	public boolean validate(String msisdn);

	public boolean validateMaxAttempt(String msisdn);
}
