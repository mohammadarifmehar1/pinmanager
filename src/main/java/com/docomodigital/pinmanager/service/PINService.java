package com.docomodigital.pinmanager.service;

public interface PINService {

	public char[] generatePIN(String msisdn) throws Exception;

	public boolean validatePIN(String msisdn, String pin);
}
