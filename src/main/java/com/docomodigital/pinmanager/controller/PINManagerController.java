package com.docomodigital.pinmanager.controller;

import java.text.MessageFormat;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.docomodigital.pinmanager.exception.PINManagerException;
import com.docomodigital.pinmanager.model.PINDto;
import com.docomodigital.pinmanager.model.PINResponse;
import com.docomodigital.pinmanager.service.PINService;

@RestController
@RequestMapping("/pin-manager")
public class PINManagerController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PINManagerController.class);

	private static final String GENERATE_RESPONSE = "GENERATE";
	private static final String VALIDATE_MESSAGE = "VALIDATE";
	private static final String SUCCESS_MESSAGE = "SUCCESS";
	private static final String FAILED_MESSAGE = "FAILED";
	
	@Autowired
	private PINService pinService;
	
	@PostMapping("/generate")
	public PINResponse generatePIN(@RequestBody PINDto pinDto) {
		PINResponse response = null;
		try {
			char[] pin = pinService.generatePIN(pinDto.getMsisdn());
			response = new PINResponse(pinDto.getMsisdn(), GENERATE_RESPONSE, SUCCESS_MESSAGE, MessageFormat.format("PIN: {0} generated successfully", Arrays.toString(pin)));
		} catch (PINManagerException e) {
			// print error msg into log file then return
			LOGGER.error("Exception while generating pin", e);
			response = new PINResponse(pinDto.getMsisdn(), GENERATE_RESPONSE, FAILED_MESSAGE, e.getErrorMessage());
		} catch (Exception e) {
			LOGGER.error("Un expected exception while generating pin", e);
			response = new PINResponse(pinDto.getMsisdn(), GENERATE_RESPONSE, FAILED_MESSAGE, "UnExpected Error, Please check db configuration for msisdn");
		}
		return response;
	}
	
	@PutMapping("/validate")
	public PINResponse validatePIN(@RequestBody PINDto pinDto) {
		PINResponse response = null;
		try {
			if(pinService.validatePIN(pinDto.getMsisdn(), pinDto.getPin())) {
				response = new PINResponse(pinDto.getMsisdn(), VALIDATE_MESSAGE, SUCCESS_MESSAGE, MessageFormat.format("PIN: {0} validated successfully", pinDto.getPin()));
			}else {
				response = new PINResponse(pinDto.getMsisdn(), VALIDATE_MESSAGE, FAILED_MESSAGE, MessageFormat.format("PIN: {0} validation failed", pinDto.getPin()));
			}
		} catch (Exception e) {
			LOGGER.error("Exception while validating pin for msisdn:{}", pinDto.getMsisdn(), e);
			response = new PINResponse(pinDto.getMsisdn(), VALIDATE_MESSAGE, FAILED_MESSAGE, MessageFormat.format("PIN: {0} validation failed, Unexpected error!!", pinDto.getPin()));
		}
		return response;
	}

}
