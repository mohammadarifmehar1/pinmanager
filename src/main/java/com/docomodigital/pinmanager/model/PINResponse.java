package com.docomodigital.pinmanager.model;

public class PINResponse {
	private String msisdn;
	private String responseType;
    private String status;
	private String message;
	
	
	public PINResponse(String msisdn, String responseType, String status, String message) {
		super();
		this.msisdn = msisdn;
		this.responseType = responseType;
		this.status = status;
		this.message = message;
	}


	public String getMsisdn() {
		return msisdn;
	}


	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}


	public String getResponseType() {
		return responseType;
	}


	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
