package com.docomodigital.pinmanager.schedular;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.docomodigital.pinmanager.entity.MSISDNEntity;
import com.docomodigital.pinmanager.entity.PINStatus;
import com.docomodigital.pinmanager.repository.MSISDNRepository;
import com.docomodigital.pinmanager.repository.PINStatusRepository;

@Component
@Transactional
public class CleanUpSchedular {

	private static final Logger LOGGER = LoggerFactory.getLogger(CleanUpSchedular.class);
	@Autowired
	private PINStatusRepository pinStatusRepository;
	@Autowired
	private MSISDNRepository msisdnRepository;
	
	@Scheduled(fixedDelayString = "${pinmanager.pin.delete.time}")
	public void cleanUpProcess() {
		try {
			LOGGER.info("cleaning pin process started");
			LocalDateTime expiryTime = LocalDateTime.now().minusMinutes(5);
			List<MSISDNEntity> entities = msisdnRepository.findAll();
			List<PINStatus>pinsToBeRemoved =new ArrayList<>();
			for(MSISDNEntity entity : entities) {
				List<PINStatus> pinStatus = entity.getPinStatus();
				List<PINStatus>updated = pinStatus.stream().filter(status->status.getCreateTime().isAfter(expiryTime)).collect(Collectors.toList());
				List<PINStatus>toBeRemoved = pinStatus.stream().filter(status->status.getCreateTime().isBefore(expiryTime)).collect(Collectors.toList());
				pinsToBeRemoved.addAll(toBeRemoved);
				entity.setPinStatus(updated);
			}
			if(!pinsToBeRemoved.isEmpty()) {
				msisdnRepository.saveAll(entities);
				pinStatusRepository.deleteAll(pinsToBeRemoved);
			}
			LOGGER.info("cleaning pin process ended");
		} catch (Exception e) {
			LOGGER.error("Exception while cleaning expired pin data", e);
		}
	}
}

